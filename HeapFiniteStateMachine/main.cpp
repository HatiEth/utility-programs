#include <iostream>
#include <unordered_map>
#include <string>

#include "hfinitestatemachine.h"

struct DoSomethingState : public State {
    int incrementi{0};
    float taccum{0};
    static const float TIME_REQUIRED;

    // State interface
public:
    void update(float dt)
    {
        taccum+=dt;
        if(taccum>=TIME_REQUIRED) {
            ++incrementi;
            taccum -= TIME_REQUIRED;
        }
    }
};
const float DoSomethingState::TIME_REQUIRED{1.0f};

int main()
{
    HFiniteStateMachine testMachine;
    DoSomethingState* s;
    testMachine.push(s=new DoSomethingState());
    while(1) {
        testMachine.update(0.016f);
        if(s->incrementi==1000) {
            break;
        }
    }

    return 0;
}

