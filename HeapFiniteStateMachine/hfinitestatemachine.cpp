#include "hfinitestatemachine.h"
#include <iostream>
HFiniteStateMachine::HFiniteStateMachine()
{
}

void HFiniteStateMachine::update(float dt)
{
    if(_states.empty())
        return;

    State* s = _states.top();
    s->update(dt);
}

void HFiniteStateMachine::push(State *s)
{
    _states.push(s);
}
