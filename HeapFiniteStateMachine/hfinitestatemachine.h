#ifndef HFINITESTATEMACHINE_H
#define HFINITESTATEMACHINE_H

#include <stack>

struct State {
    State() {}
    State(const State& o) =delete;
    virtual ~State() {}
    virtual void update(float dt){}
};

struct HFiniteStateMachine
{
    HFiniteStateMachine();

    void update(float dt);
    void push(State* s);

    std::stack<State*> _states;
};

#endif // HFINITESTATEMACHINE_H
