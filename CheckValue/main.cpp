#include <iostream>
#include <string>

using namespace std;


enum ConfigValues {
    Wireframe,
    Textured,
    NormalMap
};

#define WATCHEDTYPES_TEST_OUTPUT

#include "WatchedTypes.h"



using Config = WatchedType<ConfigValues>;

int main()
{
    Config::testValue<Wireframe>("Wireframe");
    Config::testValue<Textured>("Textured");
    Config::testValue<NormalMap>("NormalMap");
    Config::testValue<Textured>("Textured");
    Config::testValue<Wireframe>("Wireframe");
    return 0;
}

