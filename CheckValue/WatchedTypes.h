#ifndef WATCHEDTYPES_H
#define WATCHEDTYPES_H

template<typename T>
struct WatchedType {
    template<T E>
    static bool& get() {
        static bool _status = false;
        return _status;
    }

    template<T E>
    static void toggle() {
        get<E>() = !get<E>();
    }

#ifdef WATCHEDTYPES_TEST_OUTPUT
#include <string>
#include <iostream>
    template<T E>
    static void testValue(const std::string& prefix) {
        std::cout << prefix.c_str() << " (preToggle) = " << (WatchedType<T>::get<E>()?"true":"false") << std::endl;
        WatchedType<T>::toggle<E>();
        std::cout << prefix.c_str() << " (postToggle)= " << (WatchedType<T>::get<E>()?"true":"false") << std::endl;
    }
#endif // WATCHEDTYPES_TEST_OUTPUT
};

#endif // WATCHEDTYPES_H
