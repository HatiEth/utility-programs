#ifndef INPUTPROCESSOR_H
#define INPUTPROCESSOR_H

class Input {
public:
    Input(void);

    virtual bool isKeydown();
};


class InputProcessor
{
public:
    InputProcessor();

    ///@todo proper way to wrap inputs?
    virtual bool processInput(void) =0;
};

#endif // INPUTPROCESSOR_H
