#include "inputmultiplexer.h"

InputMultiplexer::InputMultiplexer()
{

}

void InputMultiplexer::addProcessor(InputProcessor *processor)
{
    m_processors.push_back(processor);
}

void InputMultiplexer::process()
{
    std::vector<InputProcessor*>::iterator it = m_processors.begin();

    for(;it!=m_processors.end();++it) {
        if((*it)->processInput()) {
           return;
        }
    }
}
