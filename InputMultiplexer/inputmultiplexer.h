#ifndef INPUTMULTIPLEXER_H
#define INPUTMULTIPLEXER_H

#include <vector>
#include "inputprocessor.h"

class InputMultiplexer
{
    std::vector<InputProcessor*> m_processors;
public:
    InputMultiplexer();

    void addProcessor(InputProcessor* processor);

    void process(void);
};

#endif // INPUTMULTIPLEXER_H
