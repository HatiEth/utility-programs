#include <iostream>
#include <unordered_map>
#include <string>

#include "inputmultiplexer.h"

class TrueProcessor : public InputProcessor {
    // InputProcessor interface
public:
    bool processInput() {
        std::cout << "always true" << std::endl;
        return true;
    }
};

class TestProcessor : public InputProcessor {
    // InputProcessor interface
public:
    bool processInput() {
        std::cout << "blablubb" << std::endl;
        return false;
    }
};

int main()
{
    InputMultiplexer* im = new InputMultiplexer();
    im->addProcessor(new TestProcessor);
    im->addProcessor(new TrueProcessor);
    im->addProcessor(new TestProcessor);

    im->process();

    return 0;
}

